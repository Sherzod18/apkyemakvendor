package uz.openweb.vendor.repo.models.products

data class ProductResponse(
    val `data`: ArrayList<Data>,
    val ok: Boolean
)
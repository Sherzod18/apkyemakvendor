package uz.openweb.vendor.ui.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.os.Looper

import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import io.paperdb.Paper
import kotlinx.android.synthetic.main.activity_login.*
import uz.openweb.vendor.BuildConfig
import uz.openweb.vendor.R
import uz.openweb.vendor.mvvm.Status


import uz.openweb.vendor.repo.models.auth.AuthReq

import uz.openweb.vendor.mvvm.viewModel.LoginActivityViewModel
import uz.openweb.vendor.utils.Constant
import uz.yemak.driver.base.BaseActivity
import uz.yemak.driver.repo.network.ApiClient

class LoginActivity : BaseActivity() {


    private var handler = Handler(Looper.getMainLooper())
    private var changeUrlCount = 0


    private val activityViewModel by lazy { ViewModelProvider(this).get(LoginActivityViewModel::class.java) }

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        Paper.init(this)

        buttonNext.setOnClickListener{
            auth()
        }

        text_version.setOnClickListener {
            changeUrlCount += 1
            handler.postDelayed({
                changeUrlCount = 0
            }, 5000)
            if (changeUrlCount == 10) {
                ApiClient.BASE_URL = Paper.book().read("base_url", ApiClient.PROD_URL)
                ApiClient.SERVER_TYPE = Paper.book().read("server_type", ApiClient.SERVER_TYPE)
                if (ApiClient.BASE_URL == ApiClient.PROD_URL) {
                    ApiClient.BASE_URL = ApiClient.TEST_URL
                    ApiClient.SERVER_TYPE = "development"
                } else {
                    ApiClient.BASE_URL = ApiClient.PROD_URL
                    ApiClient.SERVER_TYPE = "production"
                }
                Toast.makeText(this, ApiClient.BASE_URL, Toast.LENGTH_SHORT).show()
                Paper.book().write("base_url", ApiClient.BASE_URL)
                Paper.book().write("server_type", ApiClient.SERVER_TYPE)
                text_version.text = "${BuildConfig.VERSION_NAME} (${ApiClient.SERVER_TYPE})"
            }
        }

        text_version.text = "${BuildConfig.VERSION_NAME} (${Paper.book().read("server_type", ApiClient.SERVER_TYPE)})"
    }

    private fun auth() {
        showSnackbar(Constant.fcmToken)
        if (editTextLogin.text.toString().isNotEmpty() && editTextPass.text.toString().isNotEmpty()){

            val authReq = AuthReq(editTextLogin.text.toString(),editTextPass.text.toString())

            activityViewModel.auth(
                Constant.fcmToken,
                 authReq
            ).observe(this, Observer {
                it?.let {
                    when (it.status) {
                        Status.SUCCESS -> {
                            val data = it.data?.data
                            hideProgress()
                            Constant.token = data!!.access_token
                            Paper.book().write("token", Constant.token)
                            startActivity(Intent(this@LoginActivity, MainActivity::class.java))
                            this@LoginActivity.finish()
                        }
                        Status.ERROR -> showSnackbar(it.message.toString())
                        Status.LOADING -> showProgress()
                    }
                }
            })
        }else{
            textInputLogin.error = getString(R.string.error_Login)
            textInputPass.error = getString(R.string.error_password)
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        handler.removeCallbacksAndMessages(this)
    }
}
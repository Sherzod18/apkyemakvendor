package uz.openweb.vendor.base

import android.content.Context
import android.content.res.Resources
import android.os.Handler
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import uz.openweb.vendor.R

import java.text.DecimalFormat
import java.text.DecimalFormatSymbols

var exit = false


fun Fragment.onBackPressed() {
    if (activity != null) activity?.onBackPressed()
}

fun FragmentActivity.finishFragment() {
    supportFragmentManager.popBackStack()
}

fun Int.toDp(): Int = (this / Resources.getSystem().displayMetrics.density).toInt()

fun Int.toPx(): Int = (this * Resources.getSystem().displayMetrics.density).toInt()

fun Number.separate(groupingSeparator: Char = ' ', decimalSeparator: Char = '.'): String? {
    val nf = DecimalFormat("###,###.##")
    val dfs: DecimalFormatSymbols = nf.decimalFormatSymbols
    dfs.decimalSeparator = decimalSeparator
    dfs.groupingSeparator = groupingSeparator
    nf.decimalFormatSymbols = dfs

    return nf.format(this)
}

fun Context.toast(msg: String?, dur: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, msg, dur).show()
}

fun View.hideKeyboard(): Boolean {
    try {
        val inputMethodManager =
            context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        return inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    } catch (ignored: RuntimeException) {
    }
    return false
}

fun EditText.hideKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.showSoftInput(this, InputMethodManager.HIDE_IMPLICIT_ONLY)
}

fun EditText.showSoftKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
}



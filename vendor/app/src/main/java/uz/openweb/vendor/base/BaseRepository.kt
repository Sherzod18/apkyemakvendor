package uz.openweb.vendor.base

import uz.openweb.vendor.mvvm.Resource
import uz.openweb.vendor.repo.models.accept.AcceptResponse
import uz.openweb.vendor.repo.models.accept.ReqData
import uz.openweb.vendor.repo.models.accept.ResData
import uz.openweb.vendor.repo.models.account.AccountResponse
import uz.openweb.vendor.repo.models.auth.AuthReq
import uz.openweb.vendor.repo.models.auth.AuthResponse
import uz.openweb.vendor.repo.models.find_by_id_order.OrderViewResponse
import uz.openweb.vendor.repo.models.list_of_orders.OrderResponse
import uz.openweb.vendor.repo.models.products.ProductResponse
import uz.openweb.vendor.repo.models.reject.RejectReqData
import uz.yemak.driver.repo.models.acceptOrder.SuccessResponse

import uz.yemak.driver.repo.network.ApiClient
import uz.yemak.driver.repo.network.ResponseHandler
import java.lang.Exception

class BaseRepository {

    private val responseHandler = ResponseHandler()

    suspend fun auth(fcmToken: String,authReq: AuthReq): Resource<AuthResponse> {
        return try {
            responseHandler.handleSuccess(ApiClient.retrofitService().auth(fcmToken, authReq))
        } catch (e: Exception) {
            responseHandler.handleException(e)
        }
    }

    suspend fun getAccount(): Resource<AccountResponse> {
        return try {
            responseHandler.handleSuccess(ApiClient.retrofitService().getAccount())
        } catch (e: Exception) {
            responseHandler.handleException(e)
        }
    }

    suspend fun cloaedRes(until: Int): Resource<SuccessResponse> {
        return try {
            responseHandler.handleSuccess(ApiClient.retrofitService().closedRes(until))
        } catch (e: Exception) {
            responseHandler.handleException(e)
        }
    }

    suspend fun getAllProduct(): Resource<ProductResponse> {
        return try {
            responseHandler.handleSuccess(ApiClient.retrofitService().getAllProduct())
        } catch (e: Exception) {
            responseHandler.handleException(e)
        }
    }

    suspend fun getHistoryOrders(): Resource<OrderResponse> {
        return try {
            responseHandler.handleSuccess(ApiClient.retrofitService().getHistoryOrders())
        } catch (e: Exception) {
            responseHandler.handleException(e)
        }
    }

    suspend fun logout(): Resource<SuccessResponse> {
        return try {
            responseHandler.handleSuccess(ApiClient.retrofitService().logout())
        } catch (e: Exception) {
            responseHandler.handleException(e)
        }
    }

    suspend fun getOrders(pageNumber: Int, step:Int): Resource<OrderResponse> {
        return try {
            responseHandler.handleSuccess(ApiClient.retrofitService().getAllOrders(pageNumber, step))
        } catch (e: Exception) {
            responseHandler.handleException(e)
        }
    }

    suspend fun postAccept(reqData: ReqData): Resource<ResData> {
        return try {
            responseHandler.handleSuccess(ApiClient.retrofitService().accept(reqData))
        } catch (e: Exception) {
            responseHandler.handleException(e)
        }
    }

    suspend fun postReject(rejectReqData: RejectReqData) : Resource<ResData> {
        return try {
            responseHandler.handleSuccess(ApiClient.retrofitService().postReject(rejectReqData))
        } catch (e: Exception) {
            responseHandler.handleException(e)
        }
    }


    suspend fun getFindById(id: Int) : Resource<OrderViewResponse> {
        return try {
            responseHandler.handleSuccess(ApiClient.retrofitService().findById(id))
        } catch (e: Exception) {
            responseHandler.handleException(e)
        }
    }

    suspend fun updateTime(order_id: Int, time:String) : Resource<AcceptResponse> {
        return try {
            responseHandler.handleSuccess(ApiClient.retrofitService().updateTime(order_id, time))
        } catch (e: Exception) {
            responseHandler.handleException(e)
        }
    }


    suspend fun orderReady(order_id: Int) : Resource<SuccessResponse> {
        return try {
            responseHandler.handleSuccess(ApiClient.retrofitService().orderReady(order_id))
        } catch (e: Exception) {
            responseHandler.handleException(e)
        }
    }

}
package uz.yemak.driver.repo.network

import okhttp3.Interceptor
import okhttp3.Interceptor.Companion.invoke
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import uz.openweb.vendor.BuildConfig
import uz.openweb.vendor.utils.Constant


object ApiClient {

    var BASE_URL = "https://api.yemak.uz/"
    var PROD_URL = "https://api.yemak.uz/"
    var TEST_URL = "https://api.yemak-test.uz"
    var SERVER_TYPE = "production"

    private val loggingInterceptor = run {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.apply {
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        }
    }

    private val baseInterceptor: Interceptor = invoke { chain ->
        val newUrl = chain
            .request()
            .url
            .newBuilder()
            .build()
        val request = chain
            .request()
            .newBuilder()
            .url(newUrl)
            .build()
        return@invoke chain.proceed(request)
    }

    private val client: OkHttpClient = OkHttpClient
        .Builder()
        .addInterceptor(Interceptor {
            val request: Request = it.request().newBuilder()
                .header("X-App-Version", BuildConfig.VERSION_CODE.toString())
                .header("X-App-Platform", "android")
                .header("Accept-Language", "uz")
                .header("Authorization", Constant.token)
                .build()
            return@Interceptor it.proceed(request)
        })
        .addInterceptor(loggingInterceptor)
        .addInterceptor(baseInterceptor)
        .build()

    fun retrofitService(): ApiInterface {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
            .create(ApiInterface::class.java)
    }
}
package uz.openweb.vendor.repo.models.account

import java.io.Serializable

data class AccountResponse(
    val `data`: AccountInfo,
    val ok: Boolean
):Serializable
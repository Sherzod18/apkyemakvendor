package uz.openweb.vendor.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

open class BaseViewModel : ViewModel() {

    val fragmentsMutable = MutableLiveData<ArrayList<BaseFragment>>()

    val repository = BaseRepository()

}
package uz.openweb.vendor.repo.models.find_by_id_order

import uz.openweb.vendor.repo.models.list_of_orders.Order
import java.io.Serializable

data class OrderViewResponse(
    val `data`: Order,
    val ok: Boolean
): Serializable
package uz.openweb.vendor.ui.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import io.paperdb.Paper
import uz.openweb.vendor.R
import uz.openweb.vendor.base.BaseInterface
import uz.openweb.vendor.ui.dialogs.NoInternetDialog
import uz.openweb.vendor.utils.ConnectivityHelper
import uz.openweb.vendor.utils.Constant
import uz.yemak.driver.base.BaseActivity

class SplashActivity : BaseActivity(), BaseInterface {

    private var noInternetDialog: NoInternetDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        Paper.init(this)

        if (!ConnectivityHelper.isConnectedToNetwork(applicationContext) && !ConnectivityHelper.isOnline) {
            Constant.isInternetConnected = false
            noInternetDialog = NoInternetDialog(this, this)
            noInternetDialog?.show()
        } else {
            Constant.isInternetConnected = true
            if (noInternetDialog != null) noInternetDialog?.dismiss()
            noInternetDialog = null

            if (Paper.book().read<String>("token") != null && Paper.book().read<String>("token").isNotEmpty())
                startActivity(Intent(this, MainActivity::class.java))
            else startActivity(Intent(this, LoginActivity::class.java))

            finish()
        }
    }

    override fun onNoInternet() {
        startActivity(Intent(this, SplashActivity::class.java))
        finishAffinity()
    }
}
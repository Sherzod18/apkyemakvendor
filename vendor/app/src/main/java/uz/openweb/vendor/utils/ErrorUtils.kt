package uz.openweb.vendor.utils

import org.json.JSONObject
import retrofit2.Response
import uz.openweb.vendor.repo.models.APIError



class ErrorUtils {
    companion object {
        fun parseError(response: Response<*>): APIError {
            return when {
                (response.errorBody() != null) -> {
                    val jsonObject = JSONObject(response.errorBody()!!.string())
                    val errorObject = jsonObject.getJSONObject("error")
                    val code = errorObject.getInt("code")
                    val message = errorObject.getString("message")
                    APIError(code, message)
                }
                else -> APIError(400, "Unknown Error 400")
            }
        }

        fun getErrorResponseStringID(msg: String): Int {
            return when (msg) {
                "SESSION_NOT_FOUND" -> 2020

                else -> 0
            }
        }
    }
}
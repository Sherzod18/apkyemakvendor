package uz.openweb.vendor.ui.fragments.bottom_nav.news.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.adapter_order.view.*
import kotlinx.android.synthetic.main.fragment_order_item.view.*
import uz.openweb.vendor.databinding.AdapterOrderBinding
import uz.openweb.vendor.repo.models.list_of_orders.Order

class OrderListRecyclerAdapter(private var list: ArrayList<Order>) :
    RecyclerView.Adapter<OrderListRecyclerAdapter.VH>() {

    private lateinit var binding: AdapterOrderBinding
    var onItemClick: ((Order) -> Unit)? = null


    /* private val differCallback = object : DiffUtil.ItemCallback<Order>() {
         override fun areItemsTheSame(oldItem: Order, newItem: Order): Boolean {
             return oldItem.id == newItem.id
         }

         override fun areContentsTheSame(oldItem: Order, newItem: Order): Boolean {
             return oldItem == newItem
         }
     }

     val differ = AsyncListDiffer(this, differCallback)*/


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        binding = AdapterOrderBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return VH(binding)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        val order = list[position]
        holder.itemView.apply {
            setOnClickListener {
                onItemClick?.invoke(order)
            }
            orderNumber.text = "Buyurtma #${order.number}"
            orderPrice.text = "${order.cost_of_products} so'm"
            fullName.text = "${order.customer?.first_name} ${order.customer?.last_name}"
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class VH(private val binding: AdapterOrderBinding) : RecyclerView.ViewHolder(binding.root)

    fun setList(list: ArrayList<Order>) {
        this.list = list
        notifyDataSetChanged()
    }
}
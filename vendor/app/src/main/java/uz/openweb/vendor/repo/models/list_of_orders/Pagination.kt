package uz.openweb.vendor.repo.models.list_of_orders

import java.io.Serializable

data class Pagination(
    val available_pages: Int,
    val current_page_number: Int,
    val current_page_size: Int,
    val total_items_count: Int,
    val total_pages_count: Int
): Serializable
package uz.openweb.vendor.repo.models.accept

data class AcceptResponse(
    val `data`: Data,
    val ok: Boolean
)
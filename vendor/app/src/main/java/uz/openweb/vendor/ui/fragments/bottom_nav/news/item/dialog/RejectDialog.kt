package uz.openweb.vendor.ui.fragments.bottom_nav.news.item.dialog

import android.content.Context
import android.graphics.Color
import android.opengl.Visibility
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatDialog
import androidx.core.widget.addTextChangedListener
import kotlinx.android.synthetic.main.dialog_reject.*
import uz.openweb.vendor.R
import uz.openweb.vendor.base.BaseInterface
import uz.openweb.vendor.utils.textWatcher.MyTextWatcher
import uz.openweb.vendor.utils.textWatcher.MyTextWatcherInterface

class RejectDialog(context: Context?,var baseInterface: BaseInterface): AppCompatDialog(context, R.style.ProgressDialogTheme) , View.OnClickListener{

    private var isVisibility = true
    private var strReject = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_reject)
        initLenaer(isVisibility)

        card1.setOnClickListener (this)
        close_dialog.setOnClickListener (this)
        rejectOne.setOnClickListener(this)
        rejectSecond.setOnClickListener(this)
        rejectThree.setOnClickListener(this)
        cancel_btn.setOnClickListener(this)

        textArea.addTextChangedListener (MyTextWatcher(object : MyTextWatcherInterface{
            override fun myOnTextChanged(str: String) {
                setText(str)
            }
        }))

    }

    private fun setText(str: String) {
        if (str.isNotEmpty()){
            strReject = str
            cancel_btn.setBackgroundColor(Color.argb(255, 255,224,48))
        }
    }

    private fun initLenaer(visibility: Boolean) {
        if (visibility){
            linear1.visibility = LinearLayout.GONE
        }else{
            linear1.visibility = LinearLayout.VISIBLE
        }
    }

    private fun initLenaer2(isVisib: Boolean) {
        if (isVisib){
            linear2.visibility = LinearLayout.VISIBLE
        }else{
            linear2.visibility = LinearLayout.GONE
        }
    }

    private fun setStr(str: String){
        if (str.equals("boshqa")){
            initLenaer(true)
            initLenaer2(true)
            strReject = ""
            text_reject.text = "Sababni kiriting ..."
            text_reject.setTextColor(Color.argb(255, 117,117,117))
            cancel_btn.setBackgroundColor(Color.argb(255, 216,216,216))
        }else{
            textArea.text.clear()
            initLenaer2(false)
            setString(str)
        }
    }

    private fun setString(str: String){
        if (str.isNotEmpty()){
            this.strReject = str
            text_reject.text = str
            text_reject.setTextColor(Color.argb(255, 0,0,0))
            isVisibility = true
            initLenaer(true)
            cancel_btn.setBackgroundColor(Color.argb(255, 255,224,48))
        }
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.card1 -> {
                isVisibility = !isVisibility
                initLenaer(isVisibility)
                initLenaer2(false)
            }
            R.id.close_dialog -> {
                cancel()
            }
            R.id.rejectOne -> setStr("Maxsulot tugab qoldi")
            R.id.rejectSecond -> setStr("Ish vaqti tugadi")
            R.id.rejectThree -> setStr("boshqa")
            R.id.cancel_btn -> {
                if (strReject.isNotEmpty()){
                    baseInterface.selectCause(strReject)
                    dismiss()
                }
            }
        }
    }

}
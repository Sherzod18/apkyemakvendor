package uz.openweb.vendor.ui.dialogs

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import uz.openweb.vendor.ui.activities.MainActivity
import uz.yemak.driver.base.BaseActivity


class FirebaseDialog : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val dialogTitle = intent.getStringExtra("title") ?: ""
        val dialogMessage = intent.getStringExtra("message") ?: ""
        showAlertDialog(
            title = dialogTitle,
            message = dialogMessage,
            positiveClick = DialogInterface.OnClickListener { _, _ ->
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            },
            negativeClick = DialogInterface.OnClickListener { _, _ -> finish() },
            isCancelable = false
        )
    }
}
package uz.openweb.vendor.ui.activities

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.media.AudioAttributes
import android.net.ConnectivityManager
import android.net.Network
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.core.app.NotificationCompat
import com.google.firebase.iid.FirebaseInstanceId
import io.paperdb.Paper
import uz.openweb.vendor.R
import uz.openweb.vendor.base.BaseFragment
import uz.openweb.vendor.base.BaseInterface
import uz.openweb.vendor.databinding.ActivityMainBinding
import uz.openweb.vendor.ui.dialogs.NoInternetDialog
import uz.openweb.vendor.ui.fragments.HomeFragment
import uz.openweb.vendor.ui.fragments.bottom_nav.news.NewsOrderFragment
import uz.openweb.vendor.utils.Constant
import uz.yemak.driver.base.BaseActivity
import uz.yemak.driver.repo.network.ApiClient


class MainActivity : BaseActivity(), BaseInterface {

    private lateinit var binding: ActivityMainBinding
    private var connectivityManager: ConnectivityManager? = null
    private var networkCallback: ConnectivityManager.NetworkCallback? = null
    private var noInternetDialog: NoInternetDialog? = null
    val TAG = "MyTag"

    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            val fragment = supportFragmentManager.findFragmentById(R.id.main_container) as BaseFragment
            if (fragment is HomeFragment) putInt("selectedTab", fragment.getSelectedTab())
        }
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        val fragment = supportFragmentManager.findFragmentById(R.id.main_container) as BaseFragment
        if (fragment is HomeFragment) fragment.selectTab(savedInstanceState.getInt("selectedTab"))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel("channel_id", "Yemak Vendor", NotificationManager.IMPORTANCE_HIGH)
            notificationChannel.setShowBadge(true)
            notificationChannel.description = ""
            val att = AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                    .build()
            notificationChannel.setSound(Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + packageName + "/raw/sound"), att)
            notificationChannel.enableVibration(true)
            notificationChannel.vibrationPattern = longArrayOf(400, 400)
            notificationChannel.lockscreenVisibility = NotificationCompat.VISIBILITY_PUBLIC
            val manager = getSystemService(NotificationManager::class.java)
            manager.createNotificationChannel(notificationChannel)
        }


        if (savedInstanceState == null) replaceFragment(HomeFragment())
        Paper.init(this)
        Constant.token = Paper.book().read("token")
        ApiClient.BASE_URL = Paper.book().read("base_url", ApiClient.PROD_URL)
        Constant.isServiceWorking = Paper.book().read("service_status", false)
        FirebaseInstanceId.getInstance().instanceId
                .addOnCompleteListener { task -> Log.d(TAG, "onComplete: " + task.result!!.token) }

        if (intent != null && intent.hasExtra("key1")) {

            for (key in intent.extras!!.keySet()) {
                Log.d(TAG, "onCreate: Key: " + key + " Data: " + intent.extras!!.getString(key))

            }
        }

    }

    private fun internetListener() {
        connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        connectivityManager.let {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                networkCallback = object : ConnectivityManager.NetworkCallback() {
                    override fun onAvailable(network: Network) {
                        Constant.isInternetConnected = true
                        if (noInternetDialog != null) {
                            noInternetDialog?.dismiss()
                            noInternetDialog = null
                        }
//                        showSnackbar("Internet tiklandi!", isError = false)
                    }

                    override fun onLost(network: Network) {
                        super.onLost(network)
                        Constant.isInternetConnected = false
                        noInternetDialog = NoInternetDialog(this@MainActivity, this@MainActivity)
                        noInternetDialog?.show()
//                        showSnackbar(getString(R.string.error_no_internet_connection), isError = true)
                    }
                }
                it?.registerDefaultNetworkCallback(networkCallback!!)
            }
        }
    }

    override fun onNoInternet() {
        if (Constant.isInternetConnected) {
            startActivity(Intent(this, SplashActivity::class.java))
            finishAffinity()
        } else showSnackbar(getString(R.string.error_no_internet_connection), isError = true)
    }

    override fun onResume() {
        super.onResume()
        Constant.token = Paper.book().read("token")
        internetListener()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (networkCallback != null) connectivityManager?.unregisterNetworkCallback(networkCallback!!)
    }

    private fun replaceFragment(fragment: BaseFragment) = supportFragmentManager.beginTransaction().replace(R.id.main_container, fragment).commit()

    fun updateFragment() {
        val fragment = supportFragmentManager.findFragmentById(R.id.navigation_container) as BaseFragment
        if (fragment is NewsOrderFragment) fragment.updateList()
    }


}
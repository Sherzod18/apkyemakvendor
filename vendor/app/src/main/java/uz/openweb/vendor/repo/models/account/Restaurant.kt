package uz.openweb.vendor.repo.models.account

data class Restaurant(
    val description: String,
    val id: Int,
    val name: String,
    val photo: String,
    val tags: ArrayList<String>
)
package uz.openweb.vendor.ui.fragments.bottom_nav.ready_orders

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import uz.openweb.vendor.base.BaseFragment
import uz.openweb.vendor.base.BaseInterface
import uz.openweb.vendor.databinding.FragmentReadyAllOrderBinding
import uz.openweb.vendor.mvvm.Status
import uz.openweb.vendor.mvvm.viewModel.NewsOrderViewModel
import uz.openweb.vendor.repo.models.list_of_orders.Order
import uz.openweb.vendor.ui.fragments.bottom_nav.accepted_orders.adapters.ReadyRecyclerAdapter
import uz.openweb.vendor.ui.fragments.bottom_nav.accepted_orders.item.ReadyOrderItemFragment
import uz.openweb.vendor.utils.scrollListeners.EndlessRecyclerViewScrollListener

class ReadyAllOrderFragment : BaseFragment(), BaseInterface {

    private lateinit var binding: FragmentReadyAllOrderBinding
    private lateinit var adapter: ReadyRecyclerAdapter
    private val newsOrderViewModel by lazy { ViewModelProvider(this).get(NewsOrderViewModel::class.java) }

    private var list = ArrayList<Order>()
    private lateinit var scrollListener: EndlessRecyclerViewScrollListener


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentReadyAllOrderBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        newsOrderViewModel.orderListResponseMutableLiveData.observe(viewLifecycleOwner, Observer {
            it?.let {
                val data = it.data.orders
                list.addAll(data)
                adapter.notifyDataSetChanged()
            }
        })


        adapter.onItemClick = { order ->
            addFragment(ReadyOrderItemFragment.newInstance(order))
        }
    }

    private fun initViews() {
        binding.swipeRefreshLayout.setOnRefreshListener { fetchCurrentOrders(0, 3) }
        adapter = ReadyRecyclerAdapter(list)
        binding.recyclerView.setHasFixedSize(true)
        val linearLayoutManager = LinearLayoutManager(context)
        binding.recyclerView.layoutManager = linearLayoutManager
        binding.recyclerView.adapter = adapter
        scrollListener = object : EndlessRecyclerViewScrollListener(linearLayoutManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                fetchCurrentOrders(page, 3)
            }
        }
        binding.recyclerView.addOnScrollListener(scrollListener)
        if (list.size == 0){
            fetchCurrentOrders(0, 3)
        }
    }

    private fun fetchCurrentOrders(page: Int, step: Int) {
        if (page == 0) {
            list = ArrayList()
            adapter.setList(list)
            scrollListener.resetState()
        }
        newsOrderViewModel.getOrders(page, step).observe(viewLifecycleOwner, Observer {
            it?.let {
                when (it.status) {
                    Status.SUCCESS -> {
                        binding.swipeRefreshLayout.isRefreshing = false
                        val data = it.data
                        newsOrderViewModel.orderListResponseMutableLiveData.postValue(data)
                    }
                    Status.ERROR -> {
                        binding.swipeRefreshLayout.isRefreshing = false
                        showSnackbar(it.message.toString())
                    }
                    Status.LOADING -> binding.swipeRefreshLayout.isRefreshing = true
                }
            }
        })

    }

    fun updateList() = fetchCurrentOrders(0,3)

}
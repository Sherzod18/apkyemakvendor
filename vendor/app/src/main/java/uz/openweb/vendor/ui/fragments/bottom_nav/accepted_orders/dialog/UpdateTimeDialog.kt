package uz.openweb.vendor.ui.fragments.bottom_nav.accepted_orders.dialog

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatDialog
import androidx.cardview.widget.CardView
import kotlinx.android.synthetic.main.dialog_receive.*
import uz.openweb.vendor.R
import uz.openweb.vendor.base.BaseInterface


class UpdateTimeDialog(context: Context?, var selectTime: BaseInterface): AppCompatDialog(context, R.style.ProgressDialogTheme) , View.OnClickListener{

    private val cardList: Array<CardView?> = arrayOfNulls<CardView>(7)
    private lateinit var card_unfocus:CardView
    private var time = 99
    private val card_id = intArrayOf(
        R.id.min15,
        R.id.min20,
        R.id.min25,
        R.id.min30,
        R.id.min40,
        R.id.min50,
        R.id.soat1,
    )


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_update_time)
        initViews()


        next_btn.setOnClickListener (this)

        close_dialog.setOnClickListener (this)
    }

    private fun initViews() {
        for (i in 0 until this.cardList.size){
            cardList[i] = findViewById(card_id[i])
            cardList[i]?.setCardBackgroundColor(Color.WHITE)
            cardList[i]?.setOnClickListener(this)
        }

        card_unfocus = cardList[0]!!
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.min15 -> {
                time = 60 * 15
                setFocus(card_unfocus, cardList[0]!!)
            }
            R.id.min20 -> {
                time = 60 * 20
                setFocus(card_unfocus, cardList[1]!!)
            }
            R.id.min25 -> {
                time = 60 * 25
                setFocus(card_unfocus, cardList[2]!!)
            }
            R.id.min30 -> {
                time = 60 * 30
                setFocus(card_unfocus, cardList[3]!!)
            }
            R.id.min40 -> {
                time = 60 * 40
                setFocus(card_unfocus, cardList[4]!!)
            }
            R.id.min50 -> {
                time = 60 * 50
                setFocus(card_unfocus, cardList[5]!!)
            }
            R.id.soat1 -> {
                time = 60 * 60
                setFocus(card_unfocus, cardList[6]!!)
            }
            R.id.next_btn -> {
                if (time != 99){
                    selectTime.selectTime(time)
                    dismiss()
                }
            }
            R.id.close_dialog -> {cancel()}
        }
    }


    private fun setFocus(btn_unfocus: CardView, card_focus: CardView) {
        btn_unfocus.setCardBackgroundColor(Color.WHITE)
        card_focus.setCardBackgroundColor(Color.argb(255, 255,224,48))
        this.card_unfocus = card_focus
        next_btn.setBackgroundColor(Color.argb(255, 255,224,48))
    }

}

package uz.openweb.vendor.repo.models.list_of_orders

import java.io.Serializable

data class Destination(
    val address: Any,
    val lat: Double,
    val long: Double,
    val maps_image: String
): Serializable
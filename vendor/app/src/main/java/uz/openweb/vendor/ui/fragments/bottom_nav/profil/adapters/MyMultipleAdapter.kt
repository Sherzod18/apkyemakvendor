package uz.openweb.vendor.ui.fragments.bottom_nav.profil.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.adapter_history_item.view.*
import kotlinx.android.synthetic.main.adapter_time_item.view.*
import uz.openweb.vendor.R
import uz.openweb.vendor.repo.models.history.HistoryOrder
import uz.openweb.vendor.repo.models.list_of_orders.Order

class MyMultipleAdapter(private var list: ArrayList<HistoryOrder>) : RecyclerView.Adapter<RecyclerView.ViewHolder>(){

    private val VIEW_TYPE_ONE = 1
    private val VIEW_TYPE_TWO = 2

    var onItemClick: ((Order) -> Unit)? = null

    inner class TimeVH(itemView: View) : RecyclerView.ViewHolder(itemView){
        fun onBindTime(historyOrder: HistoryOrder){
            itemView.timeHistory.text = historyOrder.time
            itemView.historyOrderNumber1.text = "Buyurtma #${historyOrder.order.number}"
            itemView.historyOrderPrice1.text = "${historyOrder.order.cost_of_products} s"
            itemView.historyOrderCard1.setOnClickListener {
                onItemClick?.invoke(historyOrder.order)
            }
        }
    }

    inner class ItemVH(itemView: View) : RecyclerView.ViewHolder(itemView){
        fun onBindItem(historyOrder: HistoryOrder){
            itemView.historyOrderNumber.text = "Buyurtma #${historyOrder.order.number}"
            itemView.historyOrderPrice.text = "${historyOrder.order.cost_of_products} s"
            itemView.historyOrderCard.setOnClickListener {
                onItemClick?.invoke(historyOrder.order)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (VIEW_TYPE_ONE == viewType){
            return TimeVH(LayoutInflater.from(parent.context).inflate(R.layout.adapter_time_item, parent, false))
        }else{
            return ItemVH(LayoutInflater.from(parent.context).inflate(R.layout.adapter_history_item, parent, false))
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val itemViewType = getItemViewType(position)
        if (itemViewType == VIEW_TYPE_ONE){
            (holder as TimeVH).onBindTime(list[position])
        }else{
            (holder as ItemVH).onBindItem(list[position])
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun getItemViewType(position: Int): Int {
        val historyOrder = list[position]
        if (historyOrder.type == 1){
            return VIEW_TYPE_ONE
        }else{
            return VIEW_TYPE_TWO
        }
    }

    fun setData(list: ArrayList<HistoryOrder>){
        this.list = list
    }
}
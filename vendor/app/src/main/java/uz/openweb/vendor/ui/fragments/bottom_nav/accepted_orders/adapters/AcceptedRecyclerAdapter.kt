package uz.openweb.vendor.ui.fragments.bottom_nav.accepted_orders.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.adapter_accepted_item.view.*
import uz.openweb.vendor.databinding.AdapterAcceptedItemBinding
import uz.openweb.vendor.repo.models.list_of_orders.Order

class AcceptedRecyclerAdapter(private var list: ArrayList<Order>) : RecyclerView.Adapter<AcceptedRecyclerAdapter.VH>() {

    private lateinit var binding: AdapterAcceptedItemBinding
    var onItemClick: ((Order) -> Unit)? = null

    inner class VH(private val binding: AdapterAcceptedItemBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        binding = AdapterAcceptedItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return VH(binding)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: VH, position: Int) {
        val order = list[position]
        holder.itemView.apply {
            setOnClickListener {
                onItemClick?.invoke(order)
            }
            acceptOrderNumber.text = "Buyurtma #${order.number}"
            acceptOrderPrice.text = "${order.cost_of_products} s."
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun setList(list: ArrayList<Order>) {
        this.list = list
        notifyDataSetChanged()
    }

}
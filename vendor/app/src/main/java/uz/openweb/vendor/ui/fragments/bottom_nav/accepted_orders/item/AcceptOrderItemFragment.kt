package uz.openweb.vendor.ui.fragments.bottom_nav.accepted_orders.item

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import uz.openweb.vendor.R
import uz.openweb.vendor.base.BaseFragment
import uz.openweb.vendor.base.BaseInterface
import uz.openweb.vendor.base.onBackPressed
import uz.openweb.vendor.databinding.FragmentAcceptOrderItemBinding
import uz.openweb.vendor.mvvm.Status
import uz.openweb.vendor.mvvm.viewModel.NewsOrderViewModel
import uz.openweb.vendor.repo.models.list_of_orders.Order
import uz.openweb.vendor.repo.models.list_of_orders.Product
import uz.openweb.vendor.ui.activities.MainActivity
import uz.openweb.vendor.ui.fragments.bottom_nav.accepted_orders.AcceptedAllOrderFragment
import uz.openweb.vendor.ui.fragments.bottom_nav.accepted_orders.dialog.UpdateTimeDialog
import uz.openweb.vendor.ui.fragments.bottom_nav.news.adapters.ProductListRecyclerAdapter
import uz.openweb.vendor.utils.Format
import java.util.*
import kotlin.collections.ArrayList


class AcceptOrderItemFragment : BaseFragment() , View.OnClickListener{

    companion object {
        fun newInstance(order: Order): AcceptOrderItemFragment {
            val bundle = Bundle()
            bundle.putSerializable("order", order)
            val fragment = AcceptOrderItemFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    private lateinit var binding: FragmentAcceptOrderItemBinding
    private lateinit var adapter:ProductListRecyclerAdapter
    private lateinit var order: Order
    private var list = ArrayList<Product>()
    private var handler = Handler(Looper.getMainLooper())
    private lateinit var timeDate:String

    private val fragmentViewModel by lazy { ViewModelProvider(this).get(NewsOrderViewModel::class.java) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAcceptOrderItemBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() {
        order = arguments?.getSerializable("order")!! as Order


        setTime(order.cooking_time)

        list = order.products as ArrayList<Product>
        binding.ordersNumber.text = "Buyurtma #${order.number}"
        binding.ordersPrice.text = "Jami narxi: ${order.cost_of_products} so'm"
        binding.paymentType.text = paymentType(order.payment_type)
        binding.appBar.textTitle.text = "Buyurtma #${order.number}"

        binding.appBar.imageBack.setOnClickListener(this)

        binding.selectedTime.setOnClickListener(this)
        binding.orderReady.setOnClickListener(this)

        adapter = ProductListRecyclerAdapter(list)
        binding.rvOrderItem.setHasFixedSize(true)
        val layoutManager = LinearLayoutManager(context)
        val dividerItemDecoration = DividerItemDecoration(context, layoutManager.orientation)
        binding.rvOrderItem.layoutManager = layoutManager
        binding.rvOrderItem.addItemDecoration(dividerItemDecoration)
        binding.rvOrderItem.adapter = adapter
    }

    private fun setTime(cookingTime: Long?) {
        order.cooking_time = cookingTime
        if (cookingTime != null){
            timeDate = getTimeDate(cookingTime)
        }
        binding.selectedTime.text = "Tanlangan vaqt ($timeDate)"
    }


    private fun paymentType(paymentType: Int): CharSequence? {
        return when(paymentType){
            1 -> "To'lov: naqd pul"
            else -> "To'lov: naqd pul"
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        handler.removeCallbacksAndMessages(this)
    }

    fun getTimeDate(cookingTime: Long, datePattern: String = "HH:mm"): String = Format.getFormattedDate(Date(cookingTime * 1000), datePattern)


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imageBack -> onBackPressed()
            R.id.selected_time -> UpdateTimeDialog(context, object : BaseInterface{
                override fun selectTime(time: Int) {
                    val updateTime = order.cooking_time?.plus(time)
                    fragmentViewModel.updateTime(order.id, updateTime.toString()).observe(viewLifecycleOwner, Observer {
                        it?.let {
                            when (it.status) {
                                Status.SUCCESS -> {
                                    hideProgress()
                                    setTime(it.data?.data?.cooking_time?.toLong())
                                    ((activity as MainActivity).supportFragmentManager.findFragmentById(R.id.navigation_container) as AcceptedAllOrderFragment)
                                            .updateList()
                                }
                                Status.ERROR -> {
                                    showSnackbar(it.message.toString())
                                }
                                Status.LOADING -> showProgress()
                            }
                        }
                    })
                }
            }).show()
            R.id.order_ready -> {
                fragmentViewModel.orderReady(order.id).observe(viewLifecycleOwner, Observer {
                    it?.let {
                        when (it.status) {
                            Status.SUCCESS -> {
                                hideProgress()
                                ((activity as MainActivity).supportFragmentManager.findFragmentById(R.id.navigation_container) as AcceptedAllOrderFragment)
                                    .updateList()
                                onBackPressed()
                            }
                            Status.ERROR -> {
                                showSnackbar(it.message.toString())
                            }
                            Status.LOADING -> showProgress()
                        }
                    }
                })
            }
        }
    }


}
package uz.openweb.vendor.base

import android.location.Location
import java.io.Serializable
import java.util.*

interface BaseInterface : Serializable {


    fun dateRange(from: String, to: String) {}

    fun dateRange(from: Date, to: Date) {}

    fun onNoInternet() {}

    fun launchPermissionRequest() {
    }

    fun cancelPermissionRequest() {
    }

    fun selectTime(time:Int){}

    fun selectCause(str:String){}

    fun locationUpdated(location: Location?) {}

}
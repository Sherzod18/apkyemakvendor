package uz.openweb.vendor.ui.fragments.bottom_nav.profil

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import coil.api.load
import com.google.android.flexbox.FlexboxLayoutManager
import io.paperdb.Paper
import kotlinx.android.synthetic.main.fragment_profil.*
import uz.openweb.vendor.R
import uz.openweb.vendor.base.BaseFragment
import uz.openweb.vendor.base.BaseInterface
import uz.openweb.vendor.databinding.FragmentProfilBinding
import uz.openweb.vendor.mvvm.Status
import uz.openweb.vendor.mvvm.viewModel.LoginActivityViewModel
import uz.openweb.vendor.repo.models.account.AccountInfo
import uz.openweb.vendor.ui.activities.SplashActivity
import uz.openweb.vendor.ui.fragments.bottom_nav.profil.adapters.TagAdapter
import uz.openweb.vendor.ui.fragments.bottom_nav.profil.dialogs.ClosedDialog
import uz.openweb.vendor.ui.fragments.bottom_nav.profil.dialogs.ConnectDialog
import uz.openweb.vendor.ui.fragments.bottom_nav.profil.history_orders.HistoryOrdersFragment
import uz.openweb.vendor.ui.fragments.bottom_nav.profil.products.ProductsFragment
import uz.openweb.vendor.utils.Format
import java.util.*


class ProfilFragment : BaseFragment(), View.OnClickListener {

    private lateinit var binding: FragmentProfilBinding
    private lateinit var adapter: TagAdapter
    private var handler = Handler(Looper.getMainLooper())

    private val getAccountViewModel by lazy { ViewModelProvider(this).get(LoginActivityViewModel::class.java) }

    private var account: AccountInfo? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentProfilBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        getAccountViewModel.getAccountMutableLiveData.observe(viewLifecycleOwner, Observer {
            it?.let {
                account = it.data
                setAccountData()
            }
        })
    }

    private fun setAccountData() {
        val layoutManager = FlexboxLayoutManager(context)
//        layoutManager.flexDirection = FlexDirection.COLUMN
//        layoutManager.justifyContent = JustifyContent.FLEX_END
        profileRV.layoutManager = layoutManager
        adapter = TagAdapter(account!!.restaurant.tags)
        profileRV.adapter = adapter

        binding.resName.text = account!!.restaurant.name
        binding.profileImage.load(account!!.restaurant.photo) {
            crossfade(true)
            placeholder(R.drawable.placeholder_restaurant)
        }
        getWorkTime(account!!.temporarily_closed_until)
    }

    @SuppressLint("SetTextI18n")
    private fun getWorkTime(time: Int) {

        when (time) {
            0 -> {
                binding.profileWordTime.text = "Ish vaqti ${account!!.work_time.from.substring(0,5)} - ${account!!.work_time.to.substring(0, 5)} (har kuni)"
                binding.profileWordTime.setTextColor(Color.parseColor("#767676"))
            }
            -1 -> {
                binding.profileWordTime.text = "Muassasa noma'lum vaqtgacha yopilgan"
                binding.profileWordTime.setTextColor(Color.RED)
            }
            else ->{
                binding.profileWordTime.text = "Muassasa shu ${getTimeDate(account!!.temporarily_closed_until.toLong())} vaqtgacha yopiq"
                binding.profileWordTime.setTextColor(Color.parseColor("#767676"))
            }
        }
    }

    fun getTimeDate(createdAt: Long, datePattern: String = "dd.MM.yyyy, HH:mm"): String = Format.getFormattedDate(
        Date(createdAt * 1000), datePattern)

    private fun initViews() {
        binding.swipeRefreshLayout.setOnRefreshListener { fetchAccountInfo() }

        binding.exitAppItem.setOnClickListener(this)
        binding.allProducts.setOnClickListener(this)
        binding.allHistoryOrders.setOnClickListener(this)
        binding.profilePhone.setOnClickListener(this)
        binding.profilePause.setOnClickListener(this)

        if (account == null) fetchAccountInfo()
    }

    private fun fetchAccountInfo() {
        getAccountViewModel.getAccount().observe(viewLifecycleOwner, Observer {
            it?.let {
                when (it.status) {
                    Status.SUCCESS -> {
                        binding.swipeRefreshLayout.isRefreshing = false
                        val data = it.data
                        getAccountViewModel.getAccountMutableLiveData.postValue(data)
                        account = it.data?.data
                        setAccountData()
                    }
                    Status.ERROR -> {
                        binding.swipeRefreshLayout.isRefreshing = false
                        showSnackbar(it.message.toString())
                    }
                    Status.LOADING -> binding.swipeRefreshLayout.isRefreshing = true
                }
            }
        })
    }


    private fun logout() {
        showAlertDialog(
            title = getString(R.string.dialog_exit_account),
            positiveClick = { dialog, _ ->
                dialog.dismiss()
                getAccountViewModel.lagout().observe(viewLifecycleOwner, Observer {
                    it?.let {
                        when (it.status) {
                            Status.SUCCESS -> {
                                hideProgress()
                                Paper.book().write("token", "")
                                activity?.startActivity(
                                    Intent(
                                        activity,
                                        SplashActivity::class.java
                                    )
                                )
                                activity?.finishAffinity()
                            }
                            Status.ERROR -> showSnackbar(it.message.toString())
                            Status.LOADING -> showProgress()
                        }
                    }
                })
            },
            negativeClick = { dialog, _ -> dialog.dismiss() }
        )
    }

    override fun onDestroyView() {
        super.onDestroyView()
        handler.removeCallbacksAndMessages(this)
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.exitAppItem -> logout()
            R.id.allProducts -> addFragment(ProductsFragment())
            R.id.allHistoryOrders -> addFragment(HistoryOrdersFragment())
            R.id.profilePhone -> ConnectDialog(context, object : BaseInterface {
                override fun selectTime(time: Int) {
                    when (time) {
                        1 -> startActivity(
                            Intent(
                                Intent.ACTION_DIAL,
                                Uri.parse(String.format("tel:%s", "${account!!.contacts.tel}"))
                            )
                        )
                        2 -> startActivity(
                            Intent(
                                Intent.ACTION_VIEW,
                                Uri.parse("${account!!.contacts.tg}")
                            )
                        )
                    }
                }
            }).show()
            R.id.profilePause -> ClosedDialog(context, object : BaseInterface {
                override fun selectTime(time: Int) {
                    getAccountViewModel.closedRes(time).observe(viewLifecycleOwner, Observer {
                        it?.let {
                            when (it.status) {
                                Status.SUCCESS -> {
                                    hideProgress()
                                    fetchAccountInfo()
                                }
                                Status.ERROR -> showSnackbar(it.message.toString())
                                Status.LOADING -> showProgress()
                            }
                        }
                    })
                }
            }).show()
        }
    }
}
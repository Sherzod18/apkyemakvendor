package uz.openweb.vendor.repo.models.list_of_orders

import java.io.Serializable

data class Row(
    val id: Int,
    val name: String,
    val price: Int
): Serializable
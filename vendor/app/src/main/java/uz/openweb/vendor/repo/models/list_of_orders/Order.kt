package uz.openweb.vendor.repo.models.list_of_orders

import java.io.Serializable

data class Order(
    val additional_information: Any?,
    val cancel_reason: String?,
    val cancel_time: Long?,
    var cooking_time: Long?,
    val cost_of_delivery: Int,
    val cost_of_products: Int,
    val created_at: Int,
    val customer: Customer?,
    val destination: Destination,
    val driver: Driver?,
    val id: Int,
    val is_cancelled: Boolean,
    val number: String,
    val order_time: Int,
    val payment_type: Int,
    val products: List<Product>,
    val restaurant: Restaurant,
    val scheduled_time: Int,
    val status: Int,
    val step: Int
) : Serializable
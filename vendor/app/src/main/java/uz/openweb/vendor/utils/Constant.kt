package uz.openweb.vendor.utils

object Constant {
    const val TAG = "===YemakDriver"
    const val ACTION_START_SERVICE = "SERVICE_START"
    const val NOTIFICATION_NEW_ORDERS = "new_orders"
    const val pageSize = 20
    const val PERMISSIONS = 1

    var token = ""
    var fcmToken = ""
    var isInternetConnected = false
    //    var driverState = false
    var isServiceWorking = false
}
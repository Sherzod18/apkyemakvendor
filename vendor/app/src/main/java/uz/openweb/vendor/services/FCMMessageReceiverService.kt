package uz.openweb.vendor.services

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.media.AudioAttributes
import android.net.Uri
import android.os.Build
import android.util.Log
import android.widget.RemoteViews
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import uz.openweb.vendor.BuildConfig
import uz.openweb.vendor.R
import uz.openweb.vendor.ui.activities.MainActivity


class FCMMessageReceiverService : FirebaseMessagingService() {
    val TAG = "MyTag"
    private var CHANNEL_ID = ""
    private var title = "Default"
    private var message = "Default for notification"


    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        Log.d(TAG, "onMessageReceived: called")
        Log.d(TAG, "onMessageReceived: Message received from: " + remoteMessage.from)

        if (remoteMessage.notification != null) {

            CHANNEL_ID = getString(R.string.default_notification_channel_id)
            title = remoteMessage.notification!!.title.toString()
            message = remoteMessage.notification!!.body.toString()
            val notificationManager =
                    getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            val contentView = RemoteViews(packageName, R.layout.custom_push)
            contentView.setImageViewResource(R.id.image, R.mipmap.ic_launcher)
            contentView.setTextViewText(R.id.title, "Custom notification")
            contentView.setTextViewText(R.id.text, "This is a custom layout")

            val soundUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + BuildConfig.APPLICATION_ID + "/" + R.raw.sound)
            val notification = NotificationCompat.Builder(this, CHANNEL_ID)
            notification.setSmallIcon(R.mipmap.ic_launcher)
            notification.setContentTitle(title)
            notification.setContentText(message)
                    .setSmallIcon(R.drawable.ic_fcm_notification)
                    .setSound(soundUri)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                    .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                    .setStyle(NotificationCompat.BigTextStyle().bigText(message))

            val intent = Intent(this, MainActivity::class.java)
            val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
            notification.setContentIntent(pendingIntent)

            notificationManager.notify(1002, notification.build())
        }



        if (remoteMessage.data.size > 0) {
            Log.d(TAG, "onMessageReceived: Data Size: " + remoteMessage.data.size)
            for (key in remoteMessage.data.keys) {
                Log.d(TAG, "onMessageReceived Key: " + key + " Data: " + remoteMessage.data[key])
            }
            Log.d(TAG, "onMessageReceived: Data: " + remoteMessage.data.toString())
        }



    }




    override fun onDeletedMessages() {
        super.onDeletedMessages()
        Log.d(TAG, "onDeletedMessages: called")
    }

    override fun onNewToken(s: String) {
        super.onNewToken(s)
        Log.d(TAG, "onNewToken: called")
        //upload this token on the app server
    }
}
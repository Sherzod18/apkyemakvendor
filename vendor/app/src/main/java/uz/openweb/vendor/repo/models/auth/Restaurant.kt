package uz.openweb.vendor.repo.models.auth

data class Restaurant(
    val description: String,
    val id: Int,
    val name: String
)
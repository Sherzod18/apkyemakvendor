package uz.openweb.vendor.ui.fragments.bottom_nav.profil.products

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_products.*
import uz.openweb.vendor.base.BaseFragment
import uz.openweb.vendor.base.onBackPressed
import uz.openweb.vendor.databinding.FragmentProductsBinding
import uz.openweb.vendor.mvvm.Status
import uz.openweb.vendor.mvvm.viewModel.LoginActivityViewModel
import uz.openweb.vendor.repo.models.products.ProductResponse
import uz.openweb.vendor.ui.fragments.bottom_nav.profil.adapters.ProductItemApadter

class ProductsFragment : BaseFragment(){

    private lateinit var binding: FragmentProductsBinding
    private lateinit var adapter: ProductItemApadter
    private var handler = Handler(Looper.getMainLooper())

    private val getAccountViewModel by lazy { ViewModelProvider(this).get(LoginActivityViewModel::class.java) }

    private var data:ProductResponse? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentProductsBinding.inflate(inflater, container, false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        getAccountViewModel.getAllProductMutableLiveData.observe(viewLifecycleOwner, Observer {
            it?.let {
                data = it
                setProductData()
            }
        })

        binding.appBar.imageBack.setOnClickListener {
            onBackPressed()
        }
    }

    private fun initViews() {
        binding.swipeRefreshLayout.setOnRefreshListener { fetchProductInfo() }
        if (data == null) fetchProductInfo()
    }

    private fun fetchProductInfo() {
        getAccountViewModel.getAllProduct().observe(viewLifecycleOwner, Observer {
            it?.let {
                when (it.status) {
                    Status.SUCCESS -> {
                        binding.swipeRefreshLayout.isRefreshing = false
                        val res = it.data
                        getAccountViewModel.getAllProductMutableLiveData.postValue(res)
                        data = res
                        setProductData()
                    }
                    Status.ERROR -> {
                        binding.swipeRefreshLayout.isRefreshing = false
                        showSnackbar(it.message.toString())
                    }
                    Status.LOADING -> binding.swipeRefreshLayout.isRefreshing = true
                }
            }
        })
    }

    @SuppressLint("UseRequireInsteadOfGet")
    private fun setProductData() {
        adapter = ProductItemApadter(context!!, data!!.data)
        productRv.layoutManager = LinearLayoutManager(context)
        productRv.adapter = adapter

    }

    override fun onDestroyView() {
        super.onDestroyView()
        handler.removeCallbacksAndMessages(this)
    }
}
package uz.openweb.vendor.repo.models.products

data class Option(
    val created_at: Int,
    val description: Any,
    val id: Int,
    val name: String,
    val rows: List<Row>,
    val updated_at: Int
)
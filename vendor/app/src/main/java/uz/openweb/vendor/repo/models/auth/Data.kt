package uz.openweb.vendor.repo.models.auth

data class Data(
    val access_token: String,
    val id: Int,
    val restaurant: Restaurant,
    val username: String
)
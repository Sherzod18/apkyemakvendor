package uz.openweb.vendor.repo.models

data class APIError(
    val code: Int,
    val message: String
)

package uz.openweb.vendor.mvvm

enum class Status {
    SUCCESS, ERROR, LOADING
}
package uz.yemak.driver.base

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent

import android.graphics.Color

import android.view.View
import android.view.inputmethod.InputMethodManager

import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar

import uz.openweb.vendor.R


import uz.openweb.vendor.ui.dialogs.ProgressBarDialog
import uz.openweb.vendor.utils.ErrorUtils


abstract class BaseActivity : AppCompatActivity() {

    private var progressBarDialog: ProgressBarDialog? = null

    fun showProgress() {
        Thread {
            runOnUiThread {
                if (progressBarDialog != null) {
                    if (!progressBarDialog!!.isShowing) {
                        progressBarDialog = ProgressBarDialog(this@BaseActivity)
                        progressBarDialog!!.show()
                    }
                } else {
                    progressBarDialog = ProgressBarDialog(this@BaseActivity)
                    progressBarDialog!!.show()
                }
            }
        }.start()
    }

    fun hideProgress() {
        Thread { runOnUiThread { if (progressBarDialog != null) progressBarDialog!!.dismiss() } }.start()
    }

    fun hideKeyboard(view: View) {
        val imm = applicationContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm!!.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun showAlertDialog(
        title: String = "",
        message: String = "",
        positiveTitle: String = getString(R.string.yes),
        positiveClick: DialogInterface.OnClickListener? = null,
        negativeTitle: String = getString(R.string.no),
        negativeClick: DialogInterface.OnClickListener? = null,
        isCancelable: Boolean = true,
        dismissListener: DialogInterface.OnDismissListener? = null
    ) {
        val alertBuilder = AlertDialog.Builder(this)
        alertBuilder.setCancelable(isCancelable)
        if (title.isNotEmpty()) alertBuilder.setTitle(title)
        if (message.isNotEmpty()) alertBuilder.setMessage(message)
        if (positiveClick != null) alertBuilder.setPositiveButton(positiveTitle, positiveClick)
        if (negativeClick != null) alertBuilder.setNegativeButton(negativeTitle, negativeClick)
        if (dismissListener != null) alertBuilder.setOnDismissListener(dismissListener)
        val dialog = alertBuilder.create()
        dialog.show()
        dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor((Color.parseColor("#FFAA00")))
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor((Color.parseColor("#FFAA00")))
    }

    fun showSnackbar(msg: String?, isError: Boolean = true) {
        hideProgress()
        val snackbar = Snackbar.make(findViewById(android.R.id.content), "", Snackbar.LENGTH_SHORT)
        val message: String =
            if (ErrorUtils.getErrorResponseStringID(msg.toString()) == 0) msg.toString()
            else getString(ErrorUtils.getErrorResponseStringID(msg.toString()))
        when (msg) {
            "SESSION_NOT_FOUND",
            "SESSION_IS_NOT_ACTIVE",
            "SESSION_ALREADY_IN_USE" -> logout()
            "UPDATE_REQUIRED" -> {
                snackbar.duration = Snackbar.LENGTH_INDEFINITE
                snackbar.setActionTextColor(resources.getColor(android.R.color.black))
            }
        }

        snackbar.setText(message)
        snackbar.view.setBackgroundColor(ContextCompat.getColor(applicationContext, if (isError) R.color.red else R.color.green))
        snackbar.show()
    }

    private fun logout() {
        finishAffinity()
    }


}

package uz.openweb.vendor.repo.models.history

import uz.openweb.vendor.repo.models.list_of_orders.Order

data class HistoryOrder(
    var time: String,
    val order: Order,
    var type:Int
)
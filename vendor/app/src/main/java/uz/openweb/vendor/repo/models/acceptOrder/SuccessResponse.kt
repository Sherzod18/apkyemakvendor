package uz.yemak.driver.repo.models.acceptOrder

import com.google.gson.annotations.Expose

data class SuccessResponse(
    @Expose
    val ok: Boolean,
    @Expose
    val data: Boolean
)
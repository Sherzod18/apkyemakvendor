package uz.openweb.vendor.ui.fragments.bottom_nav.news.item

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import uz.openweb.vendor.R
import uz.openweb.vendor.base.BaseFragment
import uz.openweb.vendor.base.BaseInterface
import uz.openweb.vendor.base.onBackPressed
import uz.openweb.vendor.databinding.FragmentOrderItemBinding
import uz.openweb.vendor.mvvm.Status
import uz.openweb.vendor.mvvm.viewModel.NewsOrderViewModel
import uz.openweb.vendor.repo.models.accept.ReqData
import uz.openweb.vendor.repo.models.list_of_orders.Order
import uz.openweb.vendor.repo.models.list_of_orders.Product
import uz.openweb.vendor.repo.models.reject.RejectReqData
import uz.openweb.vendor.ui.activities.MainActivity
import uz.openweb.vendor.ui.fragments.bottom_nav.news.adapters.ProductListRecyclerAdapter
import uz.openweb.vendor.ui.fragments.bottom_nav.news.item.dialog.ReceiveDialog
import uz.openweb.vendor.ui.fragments.bottom_nav.news.item.dialog.RejectDialog


class OrderItemFragment: BaseFragment() , View.OnClickListener{

    companion object {
        fun newInstance(order: Order): OrderItemFragment {
            val bundle = Bundle()
            bundle.putSerializable("order", order)
            val fragment = OrderItemFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    private lateinit var binding: FragmentOrderItemBinding
    private lateinit var order: Order
    private lateinit var adapter: ProductListRecyclerAdapter
    private var list = ArrayList<Product>()
    private var handler = Handler(Looper.getMainLooper())

    private val fragmentViewModel by lazy { ViewModelProvider(this).get(NewsOrderViewModel::class.java) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentOrderItemBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        order = arguments?.getSerializable("order")!! as Order
        list = order.products as ArrayList<Product>
        binding.ordersNumber.text = "Buyurtma #${order.number}"
        binding.ordersPrice.text = "Jami narxi: ${order.cost_of_products} so'm"
        binding.paymentType.text = paymentType(order.payment_type)
        binding.appBar.textTitle.text = "Buyurtma #${order.number}"
        binding.appBar.imageBack.setOnClickListener(this)
        binding.callCenter.setOnClickListener(this)
        binding.btnCancel.setOnClickListener(this)
        binding.qabulQilishBtn.setOnClickListener(this)
        adapter = ProductListRecyclerAdapter(list)
        binding.rvOrderItem.setHasFixedSize(true)
        val layoutManager = LinearLayoutManager(context)
        val dividerItemDecoration = DividerItemDecoration(context, layoutManager.orientation)
        binding.rvOrderItem.layoutManager = layoutManager
        binding.rvOrderItem.addItemDecoration(dividerItemDecoration)
        binding.rvOrderItem.adapter = adapter
    }

    private fun paymentType(paymentType: Int): CharSequence? {
        return when(paymentType){
            1 -> "To'lov: naqd pul"
            else -> "To'lov: naqd pul"
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        handler.removeCallbacksAndMessages(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imageBack -> onBackPressed()
            R.id.qabul_qilish_btn -> {
                ReceiveDialog(context, object : BaseInterface {
                    override fun selectTime(time: Int) {
                        val currentTime = System.currentTimeMillis()/1000
                        val cookingTime = time + currentTime
                        val reqData = ReqData(cookingTime, order.id)
                        fragmentViewModel.postAccept(reqData).observe(viewLifecycleOwner, Observer {
                            it?.let {
                                when (it.status) {
                                    Status.SUCCESS -> {
                                        hideProgress()
                                        /*((activity as MainActivity).supportFragmentManager.findFragmentById(R.id.navigation_container) as NewsOrderFragment)
                                            .updateOrders(it.data?.ok)*/
                                        (activity as MainActivity).updateFragment()
                                        onBackPressed()
                                    }
                                    Status.ERROR -> {
                                        showSnackbar(it.message.toString())
                                    }
                                    Status.LOADING -> showProgress()
                                }
                            }
                        })
//                        showSnackbar("$time + ${Timestamp(System.currentTimeMillis())} +  ${Timestamp(timem*1000)}", false)
                    }

                }).show()
            }
            R.id.btn_cancel -> {
                RejectDialog(context, object : BaseInterface{
                    override fun selectCause(str: String) {
                        val rejectReqData = RejectReqData(order.id, str)
                        fragmentViewModel.postReject(rejectReqData).observe(viewLifecycleOwner, Observer {
                            it?.let {
                                when (it.status) {
                                    Status.SUCCESS -> {
                                        showSnackbar(getString(R.string.cancel_in_process), false)
                                        (activity as MainActivity).updateFragment()
                                        onBackPressed()
                                    }
                                    Status.ERROR -> {
                                        showSnackbar(it.message.toString())
                                    }
                                    Status.LOADING -> showProgress()
                                }
                            }
                        })
                    }
                }).show()
            }
            R.id.callCenter ->  startActivity(Intent(Intent.ACTION_DIAL, Uri.parse(String.format("tel:%s", "75 220 01 05"))))
        }
    }


}
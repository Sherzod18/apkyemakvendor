package uz.openweb.vendor.utils

import java.text.SimpleDateFormat
import java.util.*

class Format {
    companion object {
        fun getFormattedDate(date: Date, pattern: String = "yyyyMMdd"): String {
            val sdf = SimpleDateFormat(pattern, Locale.getDefault())
            return sdf.format(date)
        }

        fun getFormattedDate(date: String, inPattern: String = "yyyyMMdd", outPattern: String = "dd MMMM yyyy"): String {
            val df = SimpleDateFormat(inPattern, Locale.getDefault())
            val outputFormat = SimpleDateFormat(outPattern, Locale.getDefault())
            return outputFormat.format(df.parse(date))
        }

        fun formatTwoDigits(number: Int): String = if (number < 10) "0${number}" else number.toString()
    }
}
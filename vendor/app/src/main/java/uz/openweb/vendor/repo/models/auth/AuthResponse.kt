package uz.openweb.vendor.repo.models.auth

data class AuthResponse(
    val `data`: Data,
    val ok: Boolean
)
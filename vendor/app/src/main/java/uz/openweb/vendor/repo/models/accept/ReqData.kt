package uz.openweb.vendor.repo.models.accept

data class ReqData(
    val cooking_time: Long,
    val order_id: Int
)
package uz.openweb.vendor.repo.models.list_of_orders

import java.io.Serializable

data class OrderResponse(
    val `data`: Data,
    val ok: Boolean
): Serializable
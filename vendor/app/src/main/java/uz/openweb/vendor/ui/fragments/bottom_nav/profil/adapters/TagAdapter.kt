package uz.openweb.vendor.ui.fragments.bottom_nav.profil.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.adapter_tag_item.view.*
import uz.openweb.vendor.databinding.AdapterTagItemBinding

class TagAdapter(private var list: ArrayList<String>) : RecyclerView.Adapter<TagAdapter.VH>(){

    private lateinit var binding: AdapterTagItemBinding

    inner class VH(private val binding: AdapterTagItemBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        binding = AdapterTagItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return VH(binding)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.itemView.apply {
            tagItem.text = list[position]
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun setList(list: ArrayList<String>) {
        this.list = list
        notifyDataSetChanged()
    }
}
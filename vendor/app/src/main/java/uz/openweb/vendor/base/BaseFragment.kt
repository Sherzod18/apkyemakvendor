package uz.openweb.vendor.base

import android.annotation.SuppressLint
import android.app.AlertDialog

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.net.ConnectivityManager
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment

import com.google.android.material.snackbar.Snackbar

import io.paperdb.Paper
import uz.openweb.vendor.R
import uz.openweb.vendor.ui.activities.SplashActivity

import uz.openweb.vendor.utils.ErrorUtils
import uz.openweb.vendor.utils.Format
import uz.yemak.driver.base.BaseActivity


import java.util.*

abstract class BaseFragment : Fragment(){

    fun getCurrentDate(datePattern: String = "yyyyMMdd"): String = Format.getFormattedDate(Date(), datePattern)

    fun checkForInternet(): Boolean {
        val networkInfo = (context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager).activeNetworkInfo
        return if (networkInfo != null && (networkInfo.type == ConnectivityManager.TYPE_WIFI || networkInfo.type == ConnectivityManager.TYPE_MOBILE))
            true
        else {
            showSnackbar(getString(R.string.error_no_internet_connection))
            false
        }
    }


    @SuppressLint("UseRequireInsteadOfGet")
    fun hideKeyboard(view: View) {
        val imm = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm!!.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun showProgress() = (activity as BaseActivity).showProgress()

    fun hideProgress() = (activity as BaseActivity).hideProgress()


    @SuppressLint("UseRequireInsteadOfGet")
    fun addFragment(fragment: BaseFragment, container: Int = R.id.main_container, addBackStack: Boolean = true, tag: String = fragment.hashCode().toString()) {

        hideKeyboard(view!!)
        if (addBackStack) activity?.supportFragmentManager?.beginTransaction()
            ?.add(container, fragment)
            ?.addToBackStack(tag)
            ?.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
            ?.commit()
        else activity?.supportFragmentManager?.beginTransaction()
            ?.add(R.id.navigation_container, fragment)
            ?.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
            ?.commit()

    }

    fun replaceFragment(fragment: BaseFragment, container: Int) {
        activity?.supportFragmentManager?.beginTransaction()
            ?.replace(container, fragment)
            ?.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
            ?.commit()
    }


    @SuppressLint("UseRequireInsteadOfGet")
    fun showSnackbar(msg: String, isError: Boolean = true) {
        hideProgress()
        val snackbar = view?.let { Snackbar.make(it, "", Snackbar.LENGTH_LONG) }
        val message: String = if (ErrorUtils.getErrorResponseStringID(msg) == 0) msg else getString(ErrorUtils.getErrorResponseStringID(msg))
        when (msg) {
            "SESSION_NOT_FOUND",
            "SESSION_IS_NOT_ACTIVE",
            "SESSION_ALREADY_IN_USE" -> logout()
            "UPDATE_REQUIRED" -> {
                snackbar?.duration = Snackbar.LENGTH_INDEFINITE
//                if (activity is MainActivity) (activity as MainActivity).checkAppUpdate(AppUpdateType.IMMEDIATE)
//                snackbar?.setAction(getString(R.string.download_app)) { if (activity is MainActivity) (activity as MainActivity).checkAppUpdate(AppUpdateType.IMMEDIATE) }
                snackbar?.setActionTextColor(resources.getColor(android.R.color.black))
            }
        }

        snackbar?.setText(message)
        snackbar?.view?.setBackgroundColor(ContextCompat.getColor(context!!, if (isError) R.color.red else R.color.green))
        snackbar?.show()
    }

    fun showAlertDialog(
        title: String = "",
        message: String = "",
        positiveTitle: String = getString(R.string.yes),
        positiveClick: DialogInterface.OnClickListener? = null,
        negativeTitle: String = getString(R.string.no),
        negativeClick: DialogInterface.OnClickListener? = null,
        isCancelable: Boolean = true,
        dismissListener: DialogInterface.OnDismissListener? = null
    ) {
        val alertBuilder = AlertDialog.Builder(context)
        alertBuilder.setCancelable(isCancelable)
        if (title.isNotEmpty()) alertBuilder.setTitle(title)
        if (message.isNotEmpty()) alertBuilder.setMessage(message)
        if (positiveClick != null) alertBuilder.setPositiveButton(positiveTitle, positiveClick)
        if (negativeClick != null) alertBuilder.setNegativeButton(negativeTitle, negativeClick)
        if (dismissListener != null) alertBuilder.setOnDismissListener(dismissListener)
        val dialog = alertBuilder.create()
        dialog.show()
        dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor((Color.parseColor("#FFAA00")))
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor((Color.parseColor("#FFAA00")))
    }

    private fun logout() {
        Paper.book().write("token", "")
        activity?.startActivity(Intent(activity, SplashActivity::class.java))
        activity?.finishAffinity()
    }
}
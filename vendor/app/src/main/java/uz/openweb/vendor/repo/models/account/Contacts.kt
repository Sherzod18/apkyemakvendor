package uz.openweb.vendor.repo.models.account

data class Contacts(
    val tel: String,
    val tg: String
)
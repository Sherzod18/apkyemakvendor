package uz.openweb.vendor.repo.models.auth

data class AuthRequest(
    val password: String,
    val username: String
)
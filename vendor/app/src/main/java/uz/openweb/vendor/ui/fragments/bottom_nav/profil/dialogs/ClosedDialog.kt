package uz.openweb.vendor.ui.fragments.bottom_nav.profil.dialogs

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatDialog
import androidx.cardview.widget.CardView
import kotlinx.android.synthetic.main.dialog_closed.*
import uz.openweb.vendor.R
import uz.openweb.vendor.base.BaseInterface

class ClosedDialog(context: Context?, var baseInterface: BaseInterface) : AppCompatDialog(context, R.style.ProgressDialogTheme), View.OnClickListener {

    private val cardList: Array<CardView?> = arrayOfNulls<CardView>(4)
    private lateinit var card_unfocus: CardView
    private var time = 99
    private val card_id = intArrayOf(
        R.id.soat1,
        R.id.open,
        R.id.oneDay,
        R.id.unknown,
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_closed)
        initViews()

        saveBtn.setOnClickListener (this)

        closeDialog.setOnClickListener (this)
    }

    private fun initViews() {
        for (i in 0 until this.cardList.size){
            cardList[i] = findViewById(card_id[i])
            cardList[i]?.setCardBackgroundColor(Color.WHITE)
            cardList[i]?.setOnClickListener(this)
        }

        card_unfocus = cardList[0]!!
    }

    private fun setFocus(btn_unfocus: CardView, card_focus: CardView) {
        btn_unfocus.setCardBackgroundColor(Color.WHITE)
        card_focus.setCardBackgroundColor(Color.argb(255, 255,224,48))
        this.card_unfocus = card_focus
        saveBtn.setBackgroundColor(Color.argb(255, 255,224,48))
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.soat1 -> {
                time = 60 * 60
                setFocus(card_unfocus, cardList[0]!!)
            }
            R.id.open -> {
                time = 0
                setFocus(card_unfocus, cardList[1]!!)
            }
            R.id.oneDay -> {
                time = 24 * 60 * 60
                setFocus(card_unfocus, cardList[2]!!)
            }
            R.id.unknown -> {
                time = -1
                setFocus(card_unfocus, cardList[3]!!)
            }
            R.id.saveBtn -> {
                if (time != 99){
                    baseInterface.selectTime(time)
                    dismiss()
                }
            }
            R.id.closeDialog -> {cancel()}
        }
    }

}
package uz.openweb.vendor.ui.fragments.bottom_nav.news.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.adapter_order.view.*
import kotlinx.android.synthetic.main.adapter_product.view.*

import uz.openweb.vendor.databinding.AdapterProductBinding

import uz.openweb.vendor.repo.models.list_of_orders.Product

class ProductListRecyclerAdapter(private var list: ArrayList<Product>) : RecyclerView.Adapter<ProductListRecyclerAdapter.VH>(){

    private lateinit var binding: AdapterProductBinding
    var onItemClick: ((Product) -> Unit)? = null

    inner class VH(private val binding: AdapterProductBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        binding = AdapterProductBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return VH(binding)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        val product = list[position]
        val strBuild = StringBuilder()
        var summ = 0;
        product.options.forEach {option ->
            strBuild.append("${option.name}: ${option.row.name}, ")
            summ += option.row.price
        }

        holder.itemView.apply {
            productAmount.text = "x${product.amount}"
            productName.text = "${product.name}"
            productPrice.text = "${product.price + summ} so'm"
            productOptions.text = strBuild.toString()
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }
}
package uz.openweb.vendor.repo.models.products

data class Data(
    val description: String,
    val id: Int,
    val products: List<Product>,
    val title: String
)
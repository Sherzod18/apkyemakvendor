package uz.openweb.vendor.repo.models.products

data class Row(
    val created_at: Int,
    val id: Int,
    val name: String,
    val price: Any?,
    val updated_at: Int
)
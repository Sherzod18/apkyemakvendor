package uz.openweb.vendor.ui.fragments.bottom_nav.profil.dialogs

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatDialog
import kotlinx.android.synthetic.main.dialog_connect.*
import uz.openweb.vendor.R
import uz.openweb.vendor.base.BaseInterface

class ConnectDialog(context: Context?, var baseInterface: BaseInterface) : AppCompatDialog(context, R.style.ProgressDialogTheme) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_connect)

        callPhone.setOnClickListener {
            baseInterface.selectTime(1)
            dismiss()
        }

        rederictTelegram.setOnClickListener {
            baseInterface.selectTime(2)
            dismiss()
        }

        close_dialog.setOnClickListener {
            cancel()
        }
    }

}
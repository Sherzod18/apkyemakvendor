package uz.openweb.vendor.repo.models.list_of_orders

import java.io.Serializable

data class Option(
    val id: Int,
    val name: String,
    val row: Row
): Serializable
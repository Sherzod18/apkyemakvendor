package uz.openweb.vendor.mvvm.viewModel



import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import kotlinx.coroutines.Dispatchers
import uz.openweb.vendor.base.BaseViewModel
import uz.openweb.vendor.mvvm.Resource
import uz.openweb.vendor.repo.models.account.AccountResponse
import uz.openweb.vendor.repo.models.auth.AuthReq
import uz.openweb.vendor.repo.models.list_of_orders.OrderResponse
import uz.openweb.vendor.repo.models.products.ProductResponse

class LoginActivityViewModel : BaseViewModel() {

    val getAccountMutableLiveData = MutableLiveData<AccountResponse>()
    val getAllProductMutableLiveData = MutableLiveData<ProductResponse>()
    val getHistoryOrdersMutableLiveData = MutableLiveData<OrderResponse>()

    fun auth(fcmToken: String,authReq: AuthReq) = liveData(Dispatchers.IO) {
        emit(Resource.loading())
        emit(repository.auth(fcmToken, authReq))
    }

    fun getAccount() = liveData(Dispatchers.IO) {
        emit(Resource.loading())
        emit(repository.getAccount())
    }

    fun closedRes(until: Int) = liveData(Dispatchers.IO) {
        emit(Resource.loading())
        emit(repository.cloaedRes(until))
    }

    fun getAllProduct() = liveData(Dispatchers.IO) {
        emit(Resource.loading())
        emit(repository.getAllProduct())
    }

    fun getHistoryOrders() = liveData(Dispatchers.IO) {
        emit(Resource.loading())
        emit(repository.getHistoryOrders())
    }

    fun lagout() = liveData(Dispatchers.IO) {
        emit(Resource.loading())
        emit(repository.logout())
    }
}
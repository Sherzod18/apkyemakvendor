package uz.openweb.vendor.repo.models.list_of_orders

import java.io.Serializable

data class Data(
    val orders: List<Order>,
    val pagination: Pagination
): Serializable
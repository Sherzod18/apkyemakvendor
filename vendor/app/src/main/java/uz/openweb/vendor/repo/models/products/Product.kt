package uz.openweb.vendor.repo.models.products

data class Product(
    val created_at: Int,
    val description: String,
    val estimated_cooking_time: Int,
    val id: Int,
    val name: String,
    val options: List<Option>,
    val photo: String,
    val price: Int,
    val updated_at: Int
)
package uz.openweb.vendor.mvvm.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import kotlinx.coroutines.Dispatchers
import uz.openweb.vendor.base.BaseViewModel
import uz.openweb.vendor.mvvm.Resource
import uz.openweb.vendor.repo.models.accept.ReqData
import uz.openweb.vendor.repo.models.find_by_id_order.OrderViewResponse
import uz.openweb.vendor.repo.models.list_of_orders.OrderResponse
import uz.openweb.vendor.repo.models.reject.RejectReqData

class NewsOrderViewModel : BaseViewModel() {

    val orderListResponseMutableLiveData = MutableLiveData<OrderResponse>()
    val orderFindByIdMutableLiveData = MutableLiveData<OrderViewResponse>()

    fun getOrders(pageNumber: Int, step:Int) = liveData(Dispatchers.IO) {
        emit(Resource.loading())
        emit(repository.getOrders(pageNumber, step))
    }

    fun postAccept(reqData: ReqData) = liveData(Dispatchers.IO) {
        emit(Resource.loading())
        emit(repository.postAccept(reqData))
    }

    fun postReject(rejectReqData: RejectReqData) = liveData(Dispatchers.IO) {
        emit(Resource.loading())
        emit(repository.postReject(rejectReqData))
    }

    fun getFindById(id: Int) = liveData(Dispatchers.IO) {
        emit(Resource.loading())
        emit(repository.getFindById(id))
    }

    fun updateTime(order_id: Int, time:String) = liveData(Dispatchers.IO) {
        emit(Resource.loading())
        emit(repository.updateTime(order_id, time))
    }

    fun orderReady(order_id: Int) = liveData(Dispatchers.IO) {
        emit(Resource.loading())
        emit(repository.orderReady(order_id))
    }




}
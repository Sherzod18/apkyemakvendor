package uz.yemak.driver.repo.network

import retrofit2.http.*
import uz.openweb.vendor.repo.models.accept.AcceptResponse
import uz.openweb.vendor.repo.models.accept.ReqData
import uz.openweb.vendor.repo.models.accept.ResData
import uz.openweb.vendor.repo.models.account.AccountResponse
import uz.openweb.vendor.repo.models.auth.AuthReq
import uz.openweb.vendor.repo.models.auth.AuthResponse
import uz.openweb.vendor.repo.models.find_by_id_order.OrderViewResponse
import uz.openweb.vendor.repo.models.list_of_orders.OrderResponse
import uz.openweb.vendor.repo.models.products.ProductResponse
import uz.openweb.vendor.repo.models.reject.RejectReqData
import uz.openweb.vendor.utils.Constant
import uz.yemak.driver.repo.models.acceptOrder.SuccessResponse


interface ApiInterface {
    @POST("/manager/auth")
    suspend fun auth(
        @Header("X-Device-Token") fcm_token: String,
        @Body authReq: AuthReq
    ): AuthResponse

    @GET("/manager/account")
    suspend fun getAccount(): AccountResponse

    @GET("/manager/product")
    suspend fun getAllProduct(): ProductResponse

    @GET("/manager/order/history")
    suspend fun getHistoryOrders(): OrderResponse

    @GET("/manager/account/closed")
    suspend fun closedRes(
        @Query("until") until: Int
    ): SuccessResponse

    @GET("/manager/auth/logout")
    suspend fun logout(): SuccessResponse

    @GET("/manager/order")
    suspend fun getAllOrders(
        @Query("pageNumber") pageNumber: Int,
        @Query("step") step: Int,
        @Query("pageSize") pageSize: Int = Constant.pageSize
    ) : OrderResponse

    @POST("/manager/order/accept")
    suspend fun accept(@Body reqData: ReqData): ResData

    @POST("/manager/order/reject")
    suspend fun postReject(@Body rejectReqData: RejectReqData): ResData

    @GET("/manager/order/view")
    suspend fun findById(
        @Query("id") id: Int,
    ) : OrderViewResponse

    @GET("/manager/order/extend-time")
    suspend fun updateTime(
        @Query("order_id") order_id: Int,
        @Query("time") time: String,
    ) : AcceptResponse


    @GET("/manager/order/ready")
    suspend fun orderReady(
        @Query("order_id") order_id: Int,
    ) : SuccessResponse


}

package uz.openweb.vendor.repo.models.list_of_orders

import java.io.Serializable

data class Product(
    val amount: Int,
    val description: String,
    val id: Int,
    val name: String,
    val options: List<Option>,
    val photo: String,
    val price: Int
):Serializable
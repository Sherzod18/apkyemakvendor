package uz.openweb.vendor.repo.models.reject

data class RejectReqData(
    val order_id: Int,
    val reason: String
)
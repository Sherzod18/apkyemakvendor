package uz.openweb.vendor.ui.fragments.bottom_nav.profil.history_orders

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import uz.openweb.vendor.base.BaseFragment
import uz.openweb.vendor.base.onBackPressed
import uz.openweb.vendor.databinding.FragmentHistoryOrdersBinding
import uz.openweb.vendor.mvvm.Status
import uz.openweb.vendor.mvvm.viewModel.LoginActivityViewModel
import uz.openweb.vendor.repo.models.history.HistoryOrder
import uz.openweb.vendor.repo.models.list_of_orders.Data
import uz.openweb.vendor.ui.fragments.bottom_nav.profil.adapters.MyMultipleAdapter
import uz.openweb.vendor.ui.fragments.bottom_nav.profil.history_orders.item.HistoryProductItemFragment
import uz.openweb.vendor.utils.Format
import java.util.*
import kotlin.collections.ArrayList

class HistoryOrdersFragment : BaseFragment() {

    private lateinit var binding: FragmentHistoryOrdersBinding
    private val getAccountViewModel by lazy { ViewModelProvider(this).get(LoginActivityViewModel::class.java) }
    private var handler = Handler(Looper.getMainLooper())
    private var data:Data? = null
    private var list = ArrayList<HistoryOrder>()
    private lateinit var adapter:MyMultipleAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHistoryOrdersBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        getAccountViewModel.getHistoryOrdersMutableLiveData.observe(viewLifecycleOwner, Observer {
            it?.let {
                this.data = it.data
                getHistoryData(it.data)
            }
        })


        binding.appBar.imageBack.setOnClickListener {
            onBackPressed()
        }

        adapter.onItemClick = {order ->
            addFragment(HistoryProductItemFragment.newInstance(order))
        }

    }

    private fun initViews() {
        binding.swipeRefreshLayout.setOnRefreshListener { fetchHistoryInfo() }

        adapter = MyMultipleAdapter(list)
        binding.historyRv.layoutManager = LinearLayoutManager(context)
        binding.historyRv.adapter = adapter

        if (list.size == 0) fetchHistoryInfo()
    }

    private fun fetchHistoryInfo() {
        getAccountViewModel.getHistoryOrders().observe(viewLifecycleOwner, Observer {
            it?.let {
                when (it.status) {
                    Status.SUCCESS -> {
                        binding.swipeRefreshLayout.isRefreshing = false
                        val res = it.data
                        getAccountViewModel.getHistoryOrdersMutableLiveData.postValue(res)
                        getHistoryData(it.data?.data)
                    }
                    Status.ERROR -> {
                        binding.swipeRefreshLayout.isRefreshing = false
                        showSnackbar(it.message.toString())
                    }
                    Status.LOADING -> binding.swipeRefreshLayout.isRefreshing = true
                }
            }
        })
    }

    private fun getHistoryData(historyData: Data?) {
        var date:String = ""
        list = ArrayList()
        historyData?.orders?.forEach {order ->
            val timeDate = getTimeDate(order.created_at.toLong())
            if (timeDate == date){
                list.add(HistoryOrder("", order, 2))
            }else{
                date = timeDate
                list.add(HistoryOrder(timeDate, order, 1))
            }
        }

        adapter.setData(list)
        adapter.notifyDataSetChanged()
    }


    fun getTimeDate(createdAt: Long, datePattern: String = "dd.MM.yyyy"): String = Format.getFormattedDate(Date(createdAt * 1000), datePattern)


    override fun onDestroyView() {
        super.onDestroyView()
        handler.removeCallbacksAndMessages(this)
    }

}
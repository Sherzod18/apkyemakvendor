package uz.openweb.vendor.ui.fragments.bottom_nav.news

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import uz.openweb.vendor.base.BaseFragment
import uz.openweb.vendor.base.BaseInterface
import uz.openweb.vendor.databinding.FragmentNewsOrderBinding
import uz.openweb.vendor.mvvm.Status
import uz.openweb.vendor.mvvm.viewModel.NewsOrderViewModel
import uz.openweb.vendor.repo.models.list_of_orders.Order
import uz.openweb.vendor.ui.fragments.bottom_nav.news.adapters.OrderListRecyclerAdapter
import uz.openweb.vendor.ui.fragments.bottom_nav.news.item.OrderItemFragment
import uz.openweb.vendor.utils.scrollListeners.EndlessRecyclerViewScrollListener

class NewsOrderFragment : BaseFragment(), BaseInterface {

    private lateinit var binding: FragmentNewsOrderBinding
    private lateinit var adapter: OrderListRecyclerAdapter
    private val newsOrderViewModel by lazy { ViewModelProvider(this).get(NewsOrderViewModel::class.java) }

    private var list = ArrayList<Order>()
    private lateinit var scrollListener: EndlessRecyclerViewScrollListener

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentNewsOrderBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        newsOrderViewModel.orderListResponseMutableLiveData.observe(viewLifecycleOwner, Observer {
            it?.let {
                val data = it.data.orders
                list.addAll(data)
                adapter.notifyDataSetChanged()
                if (list.isNotEmpty()) {
                    binding.emptyListLayout.visibility = View.GONE
                    binding.recyclerView.visibility = View.VISIBLE
                    binding.textView.visibility = View.VISIBLE
                } else {
                    binding.emptyListLayout.visibility = View.VISIBLE
                    binding.recyclerView.visibility = View.GONE
                    binding.textView.visibility = View.GONE
                }
            }
        })


        adapter.onItemClick = { order ->
            addFragment(OrderItemFragment.newInstance(order))
        }
    }


    private fun initViews() {
        binding.emptyListLayout.visibility = View.GONE
        binding.swipeRefreshLayout.setOnRefreshListener { fetchCurrentOrders(0, 1) }
        adapter = OrderListRecyclerAdapter(list)
        val gridLayoutManager = GridLayoutManager(context, 2)
        binding.recyclerView.setHasFixedSize(true)
        binding.recyclerView.layoutManager = gridLayoutManager
        binding.recyclerView.adapter = adapter
        scrollListener = object : EndlessRecyclerViewScrollListener(gridLayoutManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                fetchCurrentOrders(page, 1)
            }
        }
        binding.recyclerView.addOnScrollListener(scrollListener)
        if (list.size == 0){
            fetchCurrentOrders(0, 1)
            binding.recyclerView.visibility = View.VISIBLE
            binding.textView.visibility = View.VISIBLE
        }
    }

    private fun fetchCurrentOrders(page: Int, step: Int) {
        if (page == 0) {
            list = ArrayList()
            adapter.setList(list)
            scrollListener.resetState()
        }
        newsOrderViewModel.getOrders(page, step).observe(viewLifecycleOwner, Observer {
            it?.let {
                when (it.status) {
                    Status.SUCCESS -> {
                        binding.swipeRefreshLayout.isRefreshing = false
                        val data = it.data
                        newsOrderViewModel.orderListResponseMutableLiveData.postValue(data)
                    }
                    Status.ERROR -> {
                        binding.swipeRefreshLayout.isRefreshing = false
                        showSnackbar(it.message.toString())
                    }
                    Status.LOADING -> binding.swipeRefreshLayout.isRefreshing = true
                }
            }
        })

    }

    fun updateList() = fetchCurrentOrders(0,1)

}
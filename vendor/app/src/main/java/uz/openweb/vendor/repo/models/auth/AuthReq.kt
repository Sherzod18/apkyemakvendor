package uz.openweb.vendor.repo.models.auth

data class AuthReq(
    val username: String,
    val password: String
)
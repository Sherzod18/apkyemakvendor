package uz.openweb.vendor.repo.models.account

data class WorkTime(
    val from: String,
    val to: String
)
package uz.openweb.vendor.repo.models.list_of_orders

import java.io.Serializable

data class Restaurant(
    val description: String,
    val id: Int,
    val logotype: String,
    val name: String,
    val photo: String
): Serializable
package uz.openweb.vendor.ui.dialogs

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import uz.openweb.vendor.R
import uz.openweb.vendor.base.BaseInterface
import uz.openweb.vendor.databinding.DialogNoInternetBinding


class NoInternetDialog(context: Context, private val baseInterface: BaseInterface) : Dialog(context, R.style.ProgressDialogTheme) {

//    private val containerView by lazy { View.inflate(context, R.layout.dialog_no_internet, null) as ViewGroup }

    private lateinit var binding: DialogNoInternetBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DialogNoInternetBinding.inflate(layoutInflater)
        setContentView(binding.root)
        if (window != null) window!!.setBackgroundDrawable(ColorDrawable(Color.WHITE))
        this.setCancelable(false)
        this.setCanceledOnTouchOutside(false)

        binding.buttonRestart.setOnClickListener { baseInterface.onNoInternet() }
    }
}
package uz.openweb.vendor.ui.fragments

import android.graphics.PorterDuff
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.tabs.TabLayout
import uz.openweb.vendor.R
import uz.openweb.vendor.base.BaseFragment
import uz.openweb.vendor.base.BaseViewModel
import uz.openweb.vendor.databinding.FragmentHomeBinding
import uz.openweb.vendor.ui.fragments.bottom_nav.accepted_orders.AcceptedAllOrderFragment
import uz.openweb.vendor.ui.fragments.bottom_nav.news.NewsOrderFragment
import uz.openweb.vendor.ui.fragments.bottom_nav.profil.ProfilFragment
import uz.openweb.vendor.ui.fragments.bottom_nav.ready_orders.ReadyAllOrderFragment

@Suppress("DEPRECATION")
class HomeFragment : BaseFragment(){
    private lateinit var binding: FragmentHomeBinding
    private lateinit var viewModel: BaseViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(BaseViewModel::class.java)
        viewModel.fragmentsMutable.observe(viewLifecycleOwner, Observer { initViews(it) })
        if (viewModel.fragmentsMutable.value == null) addFragments()
    }

    private fun addFragments() {
        val fragments = ArrayList<BaseFragment>()
        fragments.add(NewsOrderFragment())
        fragments.add(AcceptedAllOrderFragment())
        fragments.add(ReadyAllOrderFragment())
        fragments.add(ProfilFragment())
        replaceFragment(fragments[0], R.id.navigation_container)
        viewModel.fragmentsMutable.postValue(fragments)
    }

    private fun initViews(fragments: ArrayList<BaseFragment>) {
        binding.tabLayout.addTab(binding.tabLayout.newTab().setIcon(R.drawable.ic_home_nav))
        binding.tabLayout.addTab(binding.tabLayout.newTab().setIcon(R.drawable.ic_watch_nav))
        binding.tabLayout.addTab(binding.tabLayout.newTab().setIcon(R.drawable.ic_tayyor))
        binding.tabLayout.addTab(binding.tabLayout.newTab().setIcon(R.drawable.ic_user_nav))
        binding.tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {}

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                val tabIconColor = ContextCompat.getColor(context!!, R.color.greyAAA)
                tab!!.icon!!.setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN)
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                val tabIconColor = ContextCompat.getColor(context!!, R.color.primaryDark)
                tab!!.icon!!.setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN)
                replaceFragment(fragments[tab.position], R.id.navigation_container)
            }
        })
        binding.tabLayout.getTabAt(0)?.text = getString(R.string.new_order)
        binding.tabLayout.getTabAt(1)?.text = getString(R.string.tayy)
        binding.tabLayout.getTabAt(2)?.text = getString(R.string.history)
        binding.tabLayout.getTabAt(3)?.text = getString(R.string.profile)
    }

    fun getSelectedTab(): Int = binding.tabLayout.selectedTabPosition

    fun selectTab(position: Int) = binding.tabLayout.selectTab(binding.tabLayout.getTabAt(position))

}
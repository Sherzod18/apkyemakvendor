package uz.openweb.vendor.repo.models.accept

data class ResData(
    val `data`: Boolean,
    val ok: Boolean
)
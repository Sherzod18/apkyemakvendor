package uz.openweb.vendor

import android.app.Application
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.ContentResolver
import android.net.Uri
import android.os.Build
import android.util.Log
import com.google.firebase.iid.FirebaseInstanceId
import uz.openweb.vendor.utils.Constant

class ManagerApplication : Application() {


    val FCM_CHANNEL_ID = "FCM_CHANNEL_ID"

    override fun onCreate() {
        super.onCreate()

        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener { instanceIdResult ->
            Log.d(Constant.TAG, "${this.javaClass.name} fcm_token\n" + instanceIdResult.token)
            Constant.fcmToken = instanceIdResult.token
        }

        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val fcmChannel = NotificationChannel(
                    FCM_CHANNEL_ID, "FCM_Channel", NotificationManager.IMPORTANCE_DEFAULT)

            val soundUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + BuildConfig.APPLICATION_ID + "/" + R.raw.sound)
            fcmChannel.setSound(soundUri, Notification.AUDIO_ATTRIBUTES_DEFAULT)

            val manager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            manager.createNotificationChannel(fcmChannel)
        }*/
    }
}
package uz.openweb.vendor.repo.models.account

data class AccountInfo(
    val contacts: Contacts,
    val id: Int,
    val restaurant: Restaurant,
    val temporarily_closed_until: Int,
    val username: String,
    val work_time: WorkTime
)
package uz.openweb.vendor.ui.fragments.bottom_nav.profil.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.view.marginStart
import androidx.core.view.marginTop
import androidx.recyclerview.widget.RecyclerView
import coil.size.PixelSize
import uz.openweb.vendor.databinding.AdapterProductItemBinding
import uz.openweb.vendor.repo.models.products.Data


class ProductItemApadter(val context: Context, private var list: ArrayList<Data>) : RecyclerView.Adapter<ProductItemApadter.VH>(){

    private lateinit var binding: AdapterProductItemBinding

    inner class VH(val binding: AdapterProductItemBinding) : RecyclerView.ViewHolder(binding.root){
        @SuppressLint("SetTextI18n")
        fun bindData(data: Data) {
            binding.title.text = data.title


            val params = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )

            val paramsText = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                1f
            )

            val paramsText2 = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                1f
            )

            val paramsL = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            val paramsView = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                2
            )

            val linearLayout = binding.productLinearLayout

            data.products.forEach { product ->
                val productName = TextView(context)
                val productPrice = TextView(context)
                productName.text = product.name
                productName.layoutParams = paramsText
                productName.setTextColor(Color.parseColor("#000000"))
                productPrice.layoutParams = paramsText2
                productPrice.gravity = Gravity.END
                productPrice.setTextColor(Color.parseColor("#000000"))
                productPrice.text = "${product.price} so'm"

                val linearLayoutpro = LinearLayout(context)
                linearLayoutpro.layoutParams = params
                params.topMargin = 4
                params.bottomMargin = 4
                linearLayoutpro.orientation = LinearLayout.HORIZONTAL
                linearLayoutpro.addView(productName)
                linearLayoutpro.addView(productPrice)
                linearLayout.addView(linearLayoutpro)
                val linearLayoutRows = LinearLayout(context)
                paramsL.marginStart = 10
                linearLayoutRows.layoutParams = paramsL
                linearLayoutRows.orientation = LinearLayout.VERTICAL

                product.options.forEach {option ->
                    option.rows.forEach {row ->
                        val linearLayoutRowItem = LinearLayout(context)
                        linearLayoutRowItem.layoutParams = params
                        params.topMargin = 4
                        params.bottomMargin = 4
                        linearLayoutRowItem.orientation = LinearLayout.HORIZONTAL
                        val text1 = TextView(context)
                        text1.layoutParams = paramsText
                        text1.text = row.name
                        val text2 = TextView(context)
                        text2.layoutParams = paramsText2
                        text2.gravity = Gravity.END
                        if (row.price != null){
                            text2.text = "${row.price} so'm"
                        }else{
                            text2.text = "0 so'm"
                        }
                        linearLayoutRowItem.addView(text1)
                        linearLayoutRowItem.addView(text2)
                        linearLayoutRows.addView(linearLayoutRowItem)
                    }
                }

                linearLayout.addView(linearLayoutRows)
                val view = View(context)
                view.layoutParams = paramsView
                paramsView.topMargin = 2
                paramsView.bottomMargin = 2
                view.setBackgroundColor(Color.parseColor("#40979797"))
                linearLayout.addView(view)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        binding = AdapterProductItemBinding.inflate(
            LayoutInflater.from(context),
            parent,
            false
        )
        return VH(binding)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bindData(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }
}
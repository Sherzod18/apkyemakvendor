package uz.openweb.vendor.ui.fragments.bottom_nav.accepted_orders.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.adapter_ready_item.view.*
import uz.openweb.vendor.databinding.AdapterReadyItemBinding
import uz.openweb.vendor.repo.models.list_of_orders.Order

class ReadyRecyclerAdapter(private var list: ArrayList<Order>) : RecyclerView.Adapter<ReadyRecyclerAdapter.VH>() {

    private lateinit var binding: AdapterReadyItemBinding
    var onItemClick: ((Order) -> Unit)? = null

    inner class VH(private val binding: AdapterReadyItemBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        binding = AdapterReadyItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return VH(binding)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: VH, position: Int) {
        val order = list[position]
        holder.itemView.apply {
            setOnClickListener {
                onItemClick?.invoke(order)
            }
            readyOrderNumber.text = "Buyurtma #${order.number}"
            readyOrderPrice.text = "${order.cost_of_products} s."
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun setList(list: ArrayList<Order>) {
        this.list = list
        notifyDataSetChanged()
    }
}
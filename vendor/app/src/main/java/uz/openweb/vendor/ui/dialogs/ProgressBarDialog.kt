package uz.openweb.vendor.ui.dialogs

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import uz.openweb.vendor.R


class ProgressBarDialog(context: Context) : Dialog(context, R.style.ProgressDialogTheme) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(containerView)
        if (window != null) window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        this.setCancelable(true)
        this.setCanceledOnTouchOutside(true)
    }

    private val containerView by lazy {
        View.inflate(context, R.layout.dialog_progress, null) as ViewGroup
    }
}
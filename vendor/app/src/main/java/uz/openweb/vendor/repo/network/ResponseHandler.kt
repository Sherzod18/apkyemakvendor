package uz.yemak.driver.repo.network

import androidx.lifecycle.MutableLiveData
import retrofit2.HttpException
import uz.openweb.vendor.mvvm.Resource
import uz.openweb.vendor.utils.ErrorUtils


import java.net.SocketTimeoutException

enum class ErrorCodes(val code: Int) {
    SocketTimeOut(-1)
}

open class ResponseHandler {
    fun <T : Any> handleSuccess(data: T): Resource<T> {
        return Resource.success(data)
    }

    fun <T : Any> handleException(e: Exception): Resource<T> {
        return when (e) {
            is HttpException -> Resource.error(ErrorUtils.parseError(e.response()!!).message)
            is SocketTimeoutException -> Resource.error(getErrorMessage(ErrorCodes.SocketTimeOut.code))
            else -> Resource.error(getErrorMessage(Int.MAX_VALUE))
        }
    }

    private fun getErrorMessage(code: Int): String {
        return when (code) {
            ErrorCodes.SocketTimeOut.code -> "Timeout"
            401 -> "Unauthorised"
            404 -> "Not found"
            else -> "Serverga bogʻlanishda xatolik"
        }
    }
}
package uz.openweb.vendor.repo.models.list_of_orders

import java.io.Serializable

data class Driver(
    val car: String,
    val first_name: String,
    val id: Int,
    val last_name: String,
    val phone_number: String
): Serializable